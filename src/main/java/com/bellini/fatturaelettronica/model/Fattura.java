package com.bellini.fatturaelettronica.model;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author riccardo
 *
 */
public class Fattura {

    private final String percorsoFile;
    @SuppressWarnings("unused")
    private final File fileXml;

    public Fattura(String file) {
        if (StringUtils.isBlank(file)) {
            throw new IllegalArgumentException("Invalid filename \"" + file + "\"");
        }
        percorsoFile = file;
        fileXml = new File(file);
    }

    public String getPercorsoFile() {
        return percorsoFile;
    }
}

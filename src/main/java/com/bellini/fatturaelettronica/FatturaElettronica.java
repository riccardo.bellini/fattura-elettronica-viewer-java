package com.bellini.fatturaelettronica;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.bellini.fatturaelettronica.model.Fattura;
import com.bellini.fatturaelettronica.view.MainPanel;

/**
 * 
 * @author riccardo
 *
 */
public class FatturaElettronica implements Runnable {

    private String file;

    public FatturaElettronica(String file) {
        this.file = file;
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            // TODO log
            System.exit(255);
        }
        SwingUtilities.invokeLater(new FatturaElettronica(args[0]));
    }

    @Override
    public void run() {
        JFrame f = new JFrame("Fattura Elettronica");
        f.add(new MainPanel(new Fattura(file)));
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(800, 600);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}

package com.bellini.fatturaelettronica.view;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bellini.fatturaelettronica.model.Fattura;

/**
 * 
 * @author riccardo
 *
 */
public class MainPanel extends JPanel {

    private static final long serialVersionUID = -8922016202783687448L;

    @SuppressWarnings("unused")
    private Fattura fattura;

    public MainPanel(Fattura fattura) {
        super(new BorderLayout());
        this.fattura = fattura;

        JLabel fatturaFileLabel = new JLabel(fattura.getPercorsoFile(), JLabel.CENTER);
        this.add(fatturaFileLabel, BorderLayout.CENTER);
    }
}
